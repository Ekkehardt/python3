import random # Load module for random numbers

# !!! put script header here
# !!! name sources
# !!! enable headless mode by passing the script 2 parameters

## Define password charset                  # NO: space, O, i, I, l
charset = [                                 # i.e. 89 chars: 89^L security p.pw
    # lower                                 # !!! Enable BASE90
    'a', 'b', 'c', 'd', 'e', 'f', 'g',      # Avoids similar chars, yet many
    'h', 'j', 'k', 'm', 'n', 'o', 'p',      # Avoid space at start/end of pwd, middle: add again!
    'q', 'r', 's', 't', 'u', 'v', 'w',
    'x', 'y', 'z',
    # upper
    'A', 'B', 'C', 'D', 'E', 'F', 'G',
    'H', 'J', 'K', 'L', 'M', 'N', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W',
    'X', 'Y', 'Z',
    # num
    '0', '1', '2', '3', '4', '5', '6',
    '7', '8', '9',
    # spec
    '!', '§', '$', '%', '&', '/', '°',
    '(', ')', '=', '?', '{', '}', '[',
    ']', '*', '+', '~', '#', '@', '€',
    '-', '_', '<', '>', '|', '.', ',',
    ';', '.', '^'
    ] # !!! check if all chars are possible (e.g. Fritz!Box)

## Init variables
VERSION = "0.5.8"   # changed: Minimum number and length, code optimization
NUMBER = 0
LENGTH = 0
PASSWORD = ""

# !!! only in interactive mode, else only list passwords
## Show header, greet the user
print('Script passgen.py ',VERSION,'\n'
    '==========================================\n'
    "Let's make some passwords!\n"
    )

## Get number and length of passwords to create
NUMBER = input('How many passwords?\n')
print('')
NUMBER = int(NUMBER)
while NUMBER == 0:                          # Don't allow zero passwords
    NUMBER = input('Number must be at least 1!\n\nHow many passwords?\n')
    print('')
    NUMBER = int(NUMBER)
LENGTH = input('Of which length?\n')
print('')
LENGTH = int(LENGTH)
while LENGTH < 4:                           # Don't allow passwords shorter than 4 chars
    LENGTH = input('Length must be at least 4!\n\nOf which length?\n')
    print('')
    LENGTH = int(LENGTH)

## Account for singular
if NUMBER == 1:
    print('Your password is:\n')
else:
    print('Your passwords are:\n')

## Create the password(s)
for n in range(NUMBER):                                     # Loop over NUMBER
    for l in range(LENGTH):                                 # Loop over LENGTH
        PASSWORD = PASSWORD + random.choices(charset)[0]    # Build PASSWORD
    # !!! ensure 1 of each: lower, upper, num, spec
    # !!! ensure minLENGTH 4
    print(PASSWORD)                                         # OUTPUT
    PASSWORD = ""                                           # Delete previous PASSWORD

# Show footer, say bye
print('\n'
    '==========================================\n'
    "Goodbye!\n"
    )
