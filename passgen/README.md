# passgen.py

A Python 3 script to generate random passwords.

## Features

The script prompts for number an length.
It then creates the given number of passwords, each of the given length.
Characters are chosen from a BASE89 charset, and passwords are printed (directly) to stdout.

This is work in progress. Planned features are, among others:

- BASE90 passwords with spaces (not at begin or end of passwords)
- Minimum length of 4
- Allow to choose more "readable" passwords
- Block formatting of the output
- Output to a textfile (optional); via variables
- Allow for limiting the charset (to account for restrictions in certain clients)

## Systems

Linux, BSD, Cygwin, Windows, Mac, ...

## Technical information

Tested with Python 3.10.6 in Ubuntu 22.04 LTS.

Needs the `random` module.

## Legal information

Licensed under MIT License conditions; see file `LICENSE`.

E. Frank Sandig, 2023-03-16
