## metallucalc: Changelog
###### E. Frank Sandig -- 2018-03-24
- - -

- alloycalc.py		Calculate alloying masses for target composition
- vapocalc.py		Calculate standard vapor pressures
- mpcalc.py		Calculate mass percentages from compound formulae
- acticalc.py		Calculate activities in dilute Fe-based systems

...

### 0.01 - 2018-03-24
- Project extended to (atm) four specialized scripts

### 1.00 - planned for 2018-10-01
- First version to fulfill all requirements properly (one task, at least

### * Plan for 1.00
Scheduled for 2018-10
- [ ] ...

[1]: In Progress
- - -
### WISHLIST
- ...
