## metallucalc
- - -
#### A collection of Python3 scripts for metallurgical calculations

- alloycalc.py		Calculate alloying masses for target composition
- vapocalc.py		Calculate standard vapor pressures
- mpcalc.py		Calculate mass percentages from compound formulae
- acticalc.py		Calculate activities in dilute Fe-based systems
- ...

- - -

### Use in MS Windows
...

### Use in GNU/Linux
...

### Use in MacOS
...

- - -

### Platforms

Independent

Requires ... libraries.

Written with Spider3 IDE in Python 3 in Unbuntu 16.04 LTS.

Tested with ...

### References



### License

The `metallucalc` Python3 script collection is licensed under the terms of the **GNU GPLv3** license.

![](https://www.gnu.org/graphics/gplv3-127x51.png)

- - -

E. Frank Sandig
2018-03-24
