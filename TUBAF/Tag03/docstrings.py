# -*- coding: utf-8 -*-


def funktion(a, b):
    """
    Parameter a ist wert1
    Parameter b ist wert2
    """

    # hier ist der obere String ein doc string
    # kann in der Konsole über
    # funktion.__doc__ zugegriffen werden
    # oder auch über funktion?
    return a**b

c = funktion(3, 4)

# ----------------------------------------------------------------------------
# py2 vs py3

a = 1/3  # py2 0
         # py3 0.33333
# print a # py2
print(a)  # py3

di = {'a': 2, 'b': 3}

# py3
for k, v in di.items():
    print(k, v)

# py2
# for k, v in di.iteritems():
#     print k, v

# Umwandeln von python2 --> python3
# Skript 2to3 über Anaconda-Bash aufrufen, bspw: 2to3 datei.py
