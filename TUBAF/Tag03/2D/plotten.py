#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from scipy.integrate import odeint
from scipy.optimize import fmin

import matplotlib.pyplot as pl
import numpy as np

# import unserer Differentialgleichungen
from lagrange_lsg import xdd_fnc2, phidd_fnc2

# Zeitachse und Startwerte fuer den Solver
t = np.linspace(1, 10, 101)
x0 = np.array([0,  np.pi*3/4, 0, 0])

# Referenzdatensatz, auf den das Modell optimiert werden soll
target = np.load('messdaten.npy')

# Liste zum Sammeln der Zwischenergebnisse der Optimierung
...

def min_target(p):
    m2, l = p

    def rhs(z, t):
        x, phi, xd, phid = z # Entpacken
        F = 0

        xdd = xdd_fnc2(x, phi, xd, phid, F, m2, l)
        phidd = phidd_fnc2(x, phi, xd, phid, F, m2, l)

        return np.array([xd, phid, xdd, phidd])

    res = odeint(rhs, x0, t)

    # Zwischenergebnisse merken
    ...

    err = np.sum( (res[:,0]-target[:,0])**2 )

    return err


# Startwerte fuer Optimierung
p0 = [.5, .7]

# Optimierung - Ergebnis sind die zwei Parameter m2 und l
p_res = fmin(min_target, p0)

#-------------------------------------------------------------------------------
# Aufgabe 1: Messwerte, Optimierungsergebnis und Fehler (3x1 Plot)
#-------------------------------------------------------------------------------
pl.close('all')

fig = ...

# Subplot fuer Anfangswerte
ax1 = fig.add_subplot(...)
ax1.plot(..., label='Messung')
ax1.plot(t, resList[0][:,0], label='Modell')
ax1.legend()
ax1.grid()
ax1.set_ylabel('Weg x [m]')

# Subplot fuer Optimierungsergebnise
ax2 = fig.add_subplot(...)
ax2.plot(..., label='Messung')
ax2.plot(..., label='Modell optimiert')
ax2.legend()
ax2.grid()
ax2.set_ylabel('Weg x [m]')

# Subplot fuer Restfehler der Position x
ax3 = fig.add_subplot(...)
ax3.plot(...)
ax3.grid()
ax3.set_xlabel('Zeit [s]')
ax3.set_ylabel('Fehler $\epsilon$ [m]')

pl.savefig(...) # png
pl.savefig(...) # pdf

#-------------------------------------------------------------------------------
# Aufgabe 2: Verlauf der Optimierung (2D)
#-------------------------------------------------------------------------------

fig = pl.figure(figsize=(12,10))
ax = fig.add_subplot(1,1,1)

# Plotten aller Zwischenergebnisse in verschiedenen Grautoenen (0.1 < col < 0.8)
for i in range(len(resList)-1):
    colStep = 0.7/len(resList)

    ax.plot(..., color=str(0.1+(0.7-i*colStep)))

# Plotten der Messung und der Simulation
ax.plot(..., color=..., lw=4, label='Messung')
ax.plot(..., color=..., ls='--', lw=2, label='Modell optimiert')
ax.grid()
ax.legend()
ax.set_xlabel('Zeit [s]')
ax.set_ylabel('Weg x [m]')

pl.savefig(...) # png
pl.savefig(...) # pdf

#-------------------------------------------------------------------------------
# Aufgabe 2 Zusatz: Verlauf der Optimierung (3D)
#-------------------------------------------------------------------------------

#from mpl_toolkits.mplot3d import axes3d
#from matplotlib import cm
#from pylab import meshgrid
#
#fig = pl.figure()
#ax = fig.add_subplot(1,1,1, projection='3d')
#
#
#x_res = [i[:,0] for i in resList]
#
#Z = np.vstack(x_res).T
#X, Y = meshgrid(range(len(resList)), t)
#
#
#ax.plot_surface(X,Y,Z, rstride=1, cstride=1, cmap=cm.autumn)
#ax.set_xlabel('x')
#ax.set_ylabel('y')
#ax.set_zlabel('z')

