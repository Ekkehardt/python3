#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from scipy.integrate import odeint
from scipy.optimize import fmin

import matplotlib.pyplot as pl
import numpy as np

# import unserer Differentialgleichungen
from lagrange_lsg import xdd_fnc2, phidd_fnc2

# Zeitachse und Startwerte fuer den Solver
t = np.linspace(1, 10, 101)
x0 = np.array([0,  np.pi*3/4, 0, 0])

# Referenzdatensatz, auf den das Modell optimiert werden soll
target = np.load('messdaten.npy')

# Liste zum Sammeln der Zwischenergebnisse der Optimierung
resList = []  # alternativ: resList = list()

def min_target(p):
    m2, l = p

    def rhs(z, t):
        x, phi, xd, phid = z  # Entpacken
        F = 0

        xdd = xdd_fnc2(x, phi, xd, phid, F, m2, l)
        phidd = phidd_fnc2(x, phi, xd, phid, F, m2, l)

        return np.array([xd, phid, xdd, phidd])

    res = odeint(rhs, x0, t)

    # Zwischenergebnisse merken
    resList.append(res)

    err = np.sum((res[:, 0]-target[:, 0])**2)

    return err


# Startwerte fuer Optimierung
p0 = [.5, .7]

# Optimierung - Ergebnis sind die zwei Parameter m2 und l
p_res = fmin(min_target, p0)

# --------------------------------------------------------------------------
# Aufgabe 1: Messwerte, Optimierungsergebnis und Fehler (3x1 Plot)
# --------------------------------------------------------------------------
pl.close('all')

fig = pl.figure()

# Subplot fuer Anfangswerte
ax1 = fig.add_subplot(3, 1, 1)  # add_subplot(311)
ax1.plot(t, target[:, 0], label='Messung')
ax1.plot(t, resList[0][:, 0], label='Modell')
ax1.legend()
ax1.grid()
ax1.set_ylabel('Weg x [m]')

# Subplot fuer Optimierungsergebnise
ax2 = fig.add_subplot(3, 1, 2)
ax2.plot(t, target[:, 0], label='Messung')
ax2.plot(t, resList[-1][:, 0], label='Modell optimiert')
ax2.legend()
ax2.grid()
ax2.set_ylabel('Weg x [m]')

# Subplot fuer Restfehler der Position x
ax3 = fig.add_subplot(3, 1, 3)
ax3.plot(t, target[:, 0]-resList[-1][:, 0])
ax3.grid()
ax3.set_xlabel('Zeit [s]')
ax3.set_ylabel('Fehler $\epsilon$ [m]')

fig.subplots_adjust(wspace=0.2, hspace=0.2)
fig.tight_layout()

pl.savefig('modell.png')  # png
pl.savefig('modell.pdf')  # pdf

# ---------------------------------------------------------------------------
# Aufgabe 2: Verlauf der Optimierung (2D)
# ---------------------------------------------------------------------------

fig = pl.figure(figsize=(12, 10))
ax = fig.add_subplot(1, 1, 1)

# Plotten der Zwischenergebnisse in verschiedenen Grautoenen (0.1 < col < 0.8)
for i in range(len(resList)-1):
    colStep = 0.7/len(resList)

    ax.plot(t, resList[i][:, 0], color=str(0.1+(0.7-i*colStep)))

# Plotten der Messung und der Simulation
ax.plot(t, target[:, 0], color='red', lw=4, label='Messung')
ax.plot(t, resList[-1][:, 0], color='#FF9900', ls='--', lw=2, label='Modell optimiert')
ax.grid()
ax.legend()
ax.set_xlabel('Zeit [s]')
ax.set_ylabel('Weg x [m]')

pl.savefig('verlauf.png')  # png
pl.savefig('verlauf.pdf')  # pdf

# --------------------------------------------------------------------------
# Aufgabe 2 Zusatz: Verlauf der Optimierung (3D)
# --------------------------------------------------------------------------

# from mpl_toolkits.mplot3d import axes3d
# from matplotlib import cm
# from pylab import meshgrid

# fig = pl.figure()
# ax = fig.add_subplot(1,1,1, projection='3d')

# x_res = [i[:,0] for i in resList]

# Z = np.vstack(x_res).T
# X, Y = meshgrid(range(len(resList)), t)

# ax.plot_surface(X,Y,Z, rstride=1, cstride=1, cmap=cm.autumn)
# ax.set_xlabel('x')
# ax.set_ylabel('y')
# ax.set_zlabel('z')
