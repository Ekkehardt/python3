#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import butter, filtfilt

FNAME = 'tool_wobble.xlsx'

figsize = (12, 6)

DATA = pd.read_excel(FNAME)
TIME = DATA.iloc[:, 0].as_matrix()
NFORCE = DATA.iloc[:, 1].as_matrix()/1e6
SFORCEX = DATA.iloc[:, 3].as_matrix()/1e6
SFORCEY = DATA.iloc[:, 5].as_matrix()/1e6
SFORCEZ = DATA.iloc[:, 7].as_matrix()/1e6

SFORCE = np.sqrt(SFORCEX**2+SFORCEY**2+SFORCEZ**2)

plt.close('all')

plt.figure()
plt.plot(TIME, NFORCE, label='Normal Force', lw=2)
plt.plot(TIME, SFORCE, label='Shear Force', lw=2)
plt.legend()
plt.xlabel('Time in s')
plt.ylabel('Force in kN')
plt.gca().tick_params(direction='out', which='both')
plt.xlim((TIME.min(), TIME.max()))
plt.gcf().set_size_inches(figsize, forward=True)

DT = np.mean(np.diff(TIME))
print('Sampling frequency is %eHz' % (1/DT))

sp_n = np.fft.fft(NFORCE)
sp_s = np.fft.fft(SFORCE)
freq = np.fft.fftfreq(TIME.shape[-1], d=DT)
N = int(len(freq)/2)
freq = freq[0:N]

plt.figure()
plt.loglog(freq, np.abs(sp_n)[0:N], lw=2, label='|FFT(nforce)|')
plt.loglog(freq, np.abs(sp_s)[0:N], lw=2, label='|FFT(sforce)|')
plt.xlim((freq.min(), freq.max()))
plt.xlabel('Frequency in Hz')
plt.ylabel('|FFT|')
plt.legend()
plt.grid(ls='--')
plt.gca().tick_params(direction='out', which='both')
plt.gcf().set_size_inches(figsize, forward=True)

cf = 0.015
b, a = butter(3, cf, 'low')
NFORCE_f = filtfilt(b, a, NFORCE)
SFORCE_f = filtfilt(b, a, SFORCE)

sp_nf = np.fft.fft(NFORCE_f)
sp_sf = np.fft.fft(SFORCE_f)

print('Cuttoff frequency is %eHz' % (1/DT*cf))

plt.figure()
plt.plot(TIME, NFORCE_f, label='Normal Force', lw=2)
plt.plot(TIME, SFORCE_f, label='Shear Force', lw=2)
plt.legend()
plt.xlabel('Time in s')
plt.ylabel('Force in kN')
plt.gca().tick_params(direction='out', which='both')
plt.xlim((TIME.min(), TIME.max()))
plt.gcf().set_size_inches(figsize, forward=True)

plt.figure()
plt.loglog(freq, np.abs(sp_nf)[0:N], lw=2, label='|FFT(nforce)|')
plt.loglog(freq, np.abs(sp_sf)[0:N], lw=2, label='|FFT(sforce)|')
plt.xlim((freq.min(), freq.max()))
plt.xlabel('Frequency in Hz')
plt.ylabel('|FFT|')
plt.legend()
plt.grid(ls='--')
plt.gca().tick_params(direction='out', which='both')
plt.gcf().set_size_inches(figsize, forward=True)
