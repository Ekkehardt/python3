#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import vtk
from vtk.util.colors import *
from model import calcStep
from functions import calcPositions, setPokeMatrix

# -----------------------------------------------------------------------------

# Quader fuer Laufkatze mit Abmessungen x,y,z anlegen
lkSource = vtk.vtkCubeSource()
lkSource.SetXLength(0.3)
lkSource.SetYLength(0.1)
lkSource.SetZLength(0.1)

# Mapper
lkMapper = vtk.vtkPolyDataMapper()
lkMapper.SetInputConnection(lkSource.GetOutputPort())

# Actor
laufkatze = vtk.vtkLODActor()
laufkatze.SetMapper(lkMapper)

# -----------------------------------------------------------------------------

# Wuerfel fuer Last
lastSource = vtk.vtkCubeSource()
lastSource.SetXLength(0.05)
lastSource.SetYLength(0.05)
lastSource.SetZLength(0.05)

# Mapper
lastMapper = vtk.vtkPolyDataMapper()
lastMapper.SetInputConnection(lastSource.GetOutputPort())

# Actor
last = vtk.vtkLODActor()
last.SetMapper(lastMapper)

# Farbe der Last ändern
last.GetProperty().SetColor(red)

# -----------------------------------------------------------------------------

# Renderer, RenderWindow und RenderWindowInteractor
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# Actors zum Renderer hinzufuegen
ren.AddActor(laufkatze)
ren.AddActor(last)

# Hintergrundfarbe und Fenstergroesse
ren.SetBackground(0.1, 0.2, 0.4)
renWin.SetSize(500, 500)

# Mausmanipulator anpassen und Interactor initialisieren
iren.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())
iren.Initialize()

# -----------------------------------------------------------------------------


def updateScene(*args):
    '''
    Diese Funktion berechnet das System neu und aktualisiert die Szene.
    '''

    # einen Simulationsschritt berechnen
    t, y = calcStep()

    # Positionen und Orientierungen der Koerper berechnen
    r_laufkatze, T_laufkatze, r_last, T_last = calcPositions(y)

    # Koerper updaten
    setPokeMatrix(laufkatze, r_laufkatze, T_laufkatze)
    setPokeMatrix(last, r_last, T_last)

    # Bild neu rendern
    renWin.Render()


# -----------------------------------------------------------------------------

# Anlegen des Timers. Die Funktion updateScene wird jetzt alle 20ms aufgerufen
iren.AddObserver('TimerEvent', updateScene)
iren.CreateRepeatingTimer(20)

# Fenster oeffnen
iren.Start()

# Schliesst das Vtk Fenster wieder
# (q druecken oder Fenster mit Maus schliessen)
iren.GetRenderWindow().Finalize()
