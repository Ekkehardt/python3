#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import vtk


def calcPositions(y):
    '''
    Position und Orientierung der Koerper im Raum berechnen (im raumfesten
    Koordinatensystem)

    r - Position (3-Vektor)
    T - Orientierung (3x3-Matrix)

    Rueckgabewert der Funktion ist ein Tupel mit r und T von Laufkatze und Last
    '''

    # Geometrieparameter - Pendellaenge
    l = 0.5

    # Zustaende auf Poitionsebene - Geschwindigkeiten werden nicht benoetigt
    x = y[0]
    phi = y[1]

    # Lage der Laufkatze
    r_laufkatze = np.array([x, 0, 0])
    T_laufkatze = np.eye(3)

    # Lage der Last
    r_last = np.array([x+l*np.sin(phi), 0, -l*np.cos(phi)])
    T_last = np.array([[np.cos(phi), 0, -np.sin(phi)],
                       [0, 1, 0],
                       [np.sin(phi), 0,  np.cos(phi)]])

    return (r_laufkatze, T_laufkatze, r_last, T_last)


def setPokeMatrix(actor, r, T):
    '''
    Die Lage von 3D-Objekten in vtk wird mit einer sog. Poke-Matrix definiert.
    Sie ist eine 4x4-Matrix mit folgender Gestalt:

    [          ]
    [   T     r]
    [          ]
    [0  0  0  1]

    Soll die Lage eines Koerpers aktualisiert werden, muessen die Werte der
    Poke-Matrix geaendert werden. Das geschieht mit Hilfe dieser Funktion.
    '''

    # leere Matrix anlegen (ist eine 4x4 Einheitsmatrix)
    poke = vtk.vtkMatrix4x4()

    # poke-Matrix mit Werten befüllt
    for row in range(3):
        # Positionsvektor
        poke.SetElement(row, 3, r[row])

        # Orientierung
        for col in range(3):
            poke.SetElement(row, col, T[row, col])

    # Matrix an Actor übergeben
    actor.PokeMatrix(poke)
