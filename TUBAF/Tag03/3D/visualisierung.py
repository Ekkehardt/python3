#!/usr/bin/env python3

import vtk
from vtk.util.colors import *
from model import ...
from functions import ...

#-------------------------------------------------------------------------------

# Quader fuer Laufkatze mit Abmessungen x,y,z anlegen
lkSource = ...

# Mapper
lkMapper = ...
lkMapper.SetInputConnection(...)

# Actor
laufkatze = ...
laufkatze.SetMapper(...)

#-------------------------------------------------------------------------------

# Wuerfel fuer Last
lastSource = ...

# Mapper
lastMapper = ...

# Actor
last = ...

# Farbe der Last ändern
...

#-------------------------------------------------------------------------------

# Renderer, RenderWindow und RenderWindowInteractor
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# Actors zum Renderer hinzufuegen
...

# Hintergrundfarbe und Fenstergroesse
ren.SetBackground(0.1, 0.2, 0.4)
renWin.SetSize(500, 500)

# Mausmanipulator anpassen und Interactor initialisieren
iren.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())
iren.Initialize()

#-------------------------------------------------------------------------------

def updateScene(*args):
    '''
    Diese Funktion berechnet das System neu und aktualisiert die Szene.
    '''

    # einen Simulationsschritt berechnen
    t, y = ...

    # Positionen und Orientierungen der Koerper berechnen
    r_laufkatze, T_laufkatze, r_last, T_last = ...

    # Koerper updaten
    ...

    # Bild neu rendern
    renWin.Render()

#-------------------------------------------------------------------------------

# Anlegen des Timers. Die Funktion updateScene wird jetzt alle 20ms aufgerufen
iren.AddObserver('TimerEvent', updateScene)
iren.CreateRepeatingTimer(20)

# Fenster oeffnen
iren.Start()

# Schliesst das Vtk Fenster wieder (q druecken oder Fenster mit Maus schliessen)
iren.GetRenderWindow().Finalize()
