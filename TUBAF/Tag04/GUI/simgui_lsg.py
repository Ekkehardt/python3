#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import PyQt5.QtCore as QtCore
import PyQt5.QtWidgets as QtGui
from PyQt5.QtGui import QDoubleValidator
import sys

from model import rhs
from scipy.integrate import odeint
from scipy import arange

import matplotlib
matplotlib.use('Qt5Agg') # Backend explizit festlegen
import matplotlib.pyplot as plt
import configparser

# QApplication Instanz wird immer benoetigt (sys.argv erst mal hinnehmen)
app = QtGui.QApplication(sys.argv)

#-------------------------------------------------------------------------------

# Dialog erzeugen
dialog = QtGui.QDialog()

# Label und Eingabefelder
mass1Label = QtGui.QLabel('Masse Laufkatze', dialog)
mass1Edit = QtGui.QLineEdit('0.8', dialog)

mass2Label = QtGui.QLabel('Masse Last', dialog)
mass2Edit = QtGui.QLineEdit('0.3', dialog)

lenLabel = QtGui.QLabel('Pendellaenge', dialog)
lenEdit = QtGui.QLineEdit('0.5', dialog)

dxLabel = QtGui.QLabel('Schrittweite', dialog)
dxEdit = QtGui.QLineEdit('0.01', dialog)

tEndLabel = QtGui.QLabel('Sim.-Dauer', dialog)
tEndEdit = QtGui.QLineEdit('10', dialog)

# Buttons
simButton = QtGui.QPushButton('Simulieren', dialog)
openButton = QtGui.QPushButton('Oeffnen', dialog)
saveButton = QtGui.QPushButton('Speichern', dialog)
exitButton = QtGui.QPushButton('Exit', dialog)

# Ausrichtung anpassen --> setAlignment
mass1Edit.setAlignment(QtCore.Qt.AlignRight)
mass2Edit.setAlignment(QtCore.Qt.AlignRight)
lenEdit.setAlignment(QtCore.Qt.AlignRight)
dxEdit.setAlignment(QtCore.Qt.AlignRight)
tEndEdit.setAlignment(QtCore.Qt.AlignRight)

# zulaessige Zeichen beschraenken - hier nur Zahlen eingeben --> setValidator
mass1Edit.setValidator(QDoubleValidator(mass1Edit))
mass2Edit.setValidator(QDoubleValidator(mass2Edit))
lenEdit.setValidator(QDoubleValidator(lenEdit))
dxEdit.setValidator(QDoubleValidator(dxEdit))
tEndEdit.setValidator(QDoubleValidator(tEndEdit))

# Layout
# Steuerelemente
layout = QtGui.QGridLayout()
layout.addWidget(mass1Label, 0, 0)
layout.addWidget(mass1Edit, 0, 1)
layout.addWidget(mass2Label, 1, 0)
layout.addWidget(mass2Edit, 1, 1)
layout.addWidget(lenLabel, 2, 0)
layout.addWidget(lenEdit, 2, 1)
layout.addWidget(dxLabel, 3, 0)
layout.addWidget(dxEdit, 3, 1)
layout.addWidget(tEndLabel, 4, 0)
layout.addWidget(tEndEdit, 4, 1)

# Buttons
layout.addWidget(simButton, 5, 1, 1, 1, QtCore.Qt.AlignRight)
layout.addWidget(openButton, 6, 1, 1, 1, QtCore.Qt.AlignRight)
layout.addWidget(saveButton, 7, 1, 1, 1, QtCore.Qt.AlignRight)
layout.addWidget(exitButton, 8, 1, 1, 1, QtCore.Qt.AlignRight)

dialog.setLayout(layout)

# Focus auf Exit
exitButton.setFocus()

def openFile():
    '''
    Oeffnet eine ini-Datei und liest die gespeicherten Daten
    '''

    # Dialog zum Oeffnen eine Datei
    filename = QtGui.QFileDialog.getOpenFileName()[0]

    # Configparser anlegen
    c = configparser.SafeConfigParser()

    # aus Datei lesen
    print('lade', filename)

    if c.read(str(filename)):
        print('OK')
    else:
        print('Keine Konfigurationsdatei geladen')

    # Werte den LineEdits zuordnen
    mass1Edit.setText(c.get('Parameter', 'm1'))
    mass2Edit.setText(c.get('Parameter', 'm2'))
    lenEdit.setText(c.get('Parameter', 'l'))

    dxEdit.setText(c.get('Simulation', 'dx'))
    tEndEdit.setText(c.get('Simulation', 'tEnd'))

def saveFile():
    '''
    Dialog zur Auswahl einer Datei oeffnen und die aktuellen Parameter darin
    abspeichern
    '''

    # Dialog fuer Dateiname
    filename = QtGui.QFileDialog.getSaveFileName()[0]

    # Configparser anlegen und Daten uebergeben
    c = configparser.SafeConfigParser()

    c.add_section('Parameter')
    c.set('Parameter', 'm1', str(mass1Edit.text()))
    c.set('Parameter', 'm2', str(mass2Edit.text()))
    c.set('Parameter', 'l', str(lenEdit.text()))

    c.add_section('Simulation')
    c.set('Simulation', 'dx', str(dxEdit.text()))
    c.set('Simulation', 'tEnd', str(tEndEdit.text()))

    # Configfile schreiben
    fid = open(filename, 'w')
    c.write(fid)
    fid.flush()
    fid.close()

def simulate():
    '''
    Diese Funktion liest die Parameter aus allen LineEdits, konvertiert sie in
    floats und führt damit die Simulation aus. Anschließend werden die
    Ergebnisse mit matplotlib dargestellt. Die Startwerte der Simulation sind
    hier noch statisch vorgegeben.
    '''

    # Werte holen
    m1 = float(str(mass1Edit.text()))
    m2 = float(str(mass2Edit.text()))
    l = float(str(lenEdit.text()))
    dx = float(str(dxEdit.text()))
    tEnd = float(str(tEndEdit.text()))

    # oder auch:
    #m = mass1Edit.text().toDouble()[0]   # gibt Tupel ala (wert, OK) zurueck

    # Zeitachse anlegen
    t = arange(0, tEnd, dx)

    # Simulation ausfuehren
    res = odeint(rhs, [0, 0.3, 0, 0], t, args=(m1, m2, l))

    # Ergebnisse plotten
    fig = plt.figure()

    # Hier muss etwas getrickst werden: wir legen einen neuen Dialog an, auf den
    # matplotlib zeichnet. Der plotDialog hat unseren Hauptdialog als parent und
    # ist modeless. Damit koennen wir beliebig viele Ergebnisfenster parallel
    # darstellen.
    plotDialog = QtGui.QDialog(dialog)
    fig.canvas.parent().setParent(plotDialog)

    # Ergebnisse für Laufkatze
    ax1 = fig.add_subplot(2, 1, 1)
    ax1.plot(t, res[:, 0], label='x')
    ax1.plot(t, res[:, 2], label='dx')
    ax1.grid(True)
    ax1.legend()
    ax1.set_ylabel('Laufkatze')

    # Ergebnisse für Last
    ax2 = fig.add_subplot(2, 1, 2)
    ax2.plot(t, res[:, 1], label='phi')
    ax2.plot(t, res[:, 3], label='dphi')

    ax2.grid(True)
    ax2.legend()
    ax2.set_xlabel('Zeit [s]')
    ax2.set_ylabel('Last')

    # Hier wird jetzt der Dialog angezeigt und nicht mehr die show-Funktion von
    # matplotlib aufgerufen!
    plotDialog.show()


# Buttons verknuepfen
simButton.clicked.connect(simulate)
exitButton.clicked.connect(dialog.close)
openButton.clicked.connect(openFile)
saveButton.clicked.connect(saveFile)

#-------------------------------------------------------------------------------

# Dialog anzeigen
dialog.exec_()
