#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import  division
import PyQt5.QtCore as QtCore
import PyQt5.QtWidgets as QtGui
import sys

from model import rhs
from scipy.integrate import odeint
from scipy import arange

import matplotlib
matplotlib.use('Qt5Agg') # Backend explizit festlegen
import matplotlib.pyplot as plt
import ConfigParser

# QApplication Instanz wird immer benoetigt (sys.argv erst mal hinnehmen)
app = QtGui.QApplication(sys.argv)

#-------------------------------------------------------------------------------

# Dialog erzeugen
dialog = QtGui.QDialog()

# Label und Eingabefelder
mass1Label = ...
mass1Edit = ...

mass2Label = ...
mass2Edit = ...

lenLabel = ...
lenEdit = ...

dxLabel = ...
dxEdit = ...

tEndLabel = ...
tEndEdit = ...

# Buttons
simButton = ...
openButton = ...
saveButton = ...
exitButton = ...

# Ausrichtung anpassen --> setAlignment
...

# zulaessige Zeichen beschraenken - hier nur Zahlen eingeben --> setValidator
...

# Layout
# Steuerelemente
layout = QtGui.QGridLayout()
layout.addWidget(...)
...

# Buttons
layout.addWidget(..., QtCore.Qt.AlignRight)
dialog.setLayout(layout)

# Focus auf Exit
exitButton.setFocus()

def openFile():
    '''
    Oeffnet eine ini-Datei und liest die gespeicherten Daten
    '''

    # Dialog zum Oeffnen eine Datei
    filename = QtGui.QFileDialog.getOpenFileName()

    # Configparser anlegen
    c = ConfigParser.SafeConfigParser()

    # aus Datei lesen
    print('lade', filename)

    if c.read(str(filename)):
        print('OK')
    else:
        print('Keine Konfigurationsdatei geladen')

    # Werte den LineEdits zuordnen
    ...

def saveFile():
    '''
    Dialog zur Auswahl einer Datei oeffnen und die aktuellen Parameter darin
    abspeichern
    '''

    # Dialog fuer Dateiname
    filename = QtGui.QFileDialog.getSaveFileName()

    # Configparser anlegen und Daten uebergeben
    c = ConfigParser.SafeConfigParser()

    c.add_section('Parameter')
    ...

    # Configfile schreiben
    fid = open(filename, 'w')
    c.write(fid)
    fid.close()

def simulate():
    '''
    Diese Funktion liest die Parameter aus allen LineEdits, konvertiert sie in
    floats und führt damit die Simulation aus. Anschließend werden die
    Ergebnisse mit matplotlib dargestellt. Die Startwerte der Simulation sind
    hier noch statisch vorgegeben.
    '''

    # Werte holen
    m1 = float(str(mass1Edit.text()))
    m2 = float(str(mass2Edit.text()))
    l = float(str(lenEdit.text()))
    dx = float(str(dxEdit.text()))
    tEnd = float(str(tEndEdit.text()))

    # oder auch:
    #m = mass1Edit.text().toDouble()[0]   # gibt Tupel ala (wert, OK) zurueck

    # Zeitachse anlegen
    t = ...

    # Simulation ausfuehren
    res = ...

    # Ergebnisse plotten
    fig = plt.figure()

    # Hier muss etwas getrickst werden: wir legen einen neuen Dialog an, auf den
    # matplotlib zeichnet. Der plotDialog hat unseren Hauptdialog als parent und
    # ist modeless. Damit koennen wir beliebig viele Ergebnisfenster parallel
    # darstellen.
    plotDialog = QtGui.QDialog(dialog)
    fig.canvas.parent().setParent(plotDialog)

    # Ergebnisse für Laufkatze
    ax1 = fig.add_subplot(2,1,1)
    ...

    ax1.grid(True)
    ax1.legend()
    ax1.set_ylabel('Laufkatze')

    # Ergebnisse für Last
    ax2 = fig.add_subplot(2,1,2)
    ...

    ax2.grid(True)
    ax2.legend()
    ax2.set_xlabel('Zeit [s]')
    ax2.set_ylabel('Last')

    # Hier wird jetzt der Dialog angezeigt und nicht mehr die show-Funktion von
    # matplotlib aufgerufen!
    plotDialog.show()

# Buttons verknuepfen
...

#-------------------------------------------------------------------------------

# Dialog anzeigen
dialog.exec_()
