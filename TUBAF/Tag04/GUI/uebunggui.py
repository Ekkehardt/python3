# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QApplication, QDialog, QLabel
from PyQt5.QtWidgets import QPushButton, QVBoxLayout, QLineEdit
import sys

def main():
    a = QApplication(sys.argv)
    
    dialog = QDialog()
    dialog.setWindowTitle('Hello World!')
    massLabel = QLabel('Masse')
    exitButton = QPushButton('Exit')
    massEdit = QLineEdit()
    
    # layout
    layout = QVBoxLayout(dialog)
    layout.addWidget(massLabel)
    layout.addWidget(exitButton)
    layout.addWidget(massEdit)
    massEdit.insert('10.5')
    massEdit.setFocus()  # eingabefokus auf lineedit
    
    exitButton.clicked.connect(dialog.close)
    dialog.exec_()
    
    # wert = massEdit.text()
    print('Masse:', massEdit.text())
    
if __name__ is '__main__':
    main()