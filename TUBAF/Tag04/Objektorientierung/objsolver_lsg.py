#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy.optimize import fmin
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from os.path import exists

# Aufgaben:
# 1. Import der xdd_fnc2 und phidd_fnc2 aus lagrange_lsg als Klassenimport
# 2. Anlegen und initialisieren der privaten Variablen im Konstruktor
# (reslist, laden der .npy nach target)
# 3. Anhängen der Optmierungsergebnisse an die Liste
# 4. Paramter der Optimierung abspeichern in die privaten Variablen m2 und le
# 5. Anlegen einer read-only Eigenschaft zu le, vgl m2
# 6. Anlegen einer Eigenschaft fSize mit den entsprechenden getter und setter
# 7. Vervollständigung der Plotfunktion entsprechend der Übung vom Vortag
# 8. Anlegen einer Instanz der Solver-Klasse
# 9. Ausgabe der opt. Parameter mit print mit Zugriff auf die Eigenschaften
# 10. Testen der __str__ Funktion der Klasse Solver mittels print


# Klasse für den Löser zum plotten
class Solver:
    # Klassenimport der funktionen aus lagrange_lsg
    from lagrange_lsg import xdd_fnc2, phidd_fnc2

    def __init__(self, filename=None):  # Konstruktor
        if filename is not None and exists(filename):
            # laden der Daten und initialisierung der Liste
            self.__target = np.load(filename)
            self.__fsize = (12, 9)
            self.__reslist = list()  # self.__reslist = []
            self.__x0 = np.array([0, np.pi*3/4, 0, 0])
            self.__t = np.linspace(1, 10, 101)
        else:
            raise IOError('File %s not found!' % filename)

    def optimize_model(self, p0=(0.5, 0.5)):  # Klassenfunktion
        self.__reslist.clear()  # vor start alle ergebnisse der Liste löschen

        def min_target(p):
            m2, le = p

            def rhs(z, t):
                x, phi, xd, phid = z
                F = 0
                xdd = Solver.xdd_fnc2(x, phi, xd, phid, F, m2, le)
                phidd = Solver.phidd_fnc2(x, phi, xd, phid, F, m2, le)

                return np.array([xd, phid, xdd, phidd])

            res = odeint(rhs, self.__x0, self.__t)
            self.__reslist.append(res)  # ergebnisse hinzufügen

            # fehler berechnen
            err = np.sum((res[:, 0]-self.__target[:, 0])**2)
            return err

        p_res = fmin(min_target, p0)
        # parameter auslesen
        self.__m2 = p_res[0]
        self.__le = p_res[1]

    # read-only eigenschaft M2
    @property
    def M2(self):
        return self.__m2

    # read-only eigenschaft Length
    @property
    def Length(self):
        return self.__le

    # eigenschaft fSize
    def _get_fsize(self):
        return self.__fsize

    def _set_fsize(self, val):
        self.__fsize = val

    fSize = property(_get_fsize, _set_fsize)

    # read-only eigenschaft reslist
    def _get_reslist(self):
        return self.__reslist

    OptiList = property(_get_reslist)

    def plotoptisteps(self):
        # self.optimize_model()
        if len(self.__reslist) == 0:
            return
        fig = plt.figure()
        ax = fig.add_subplot(111)

        colstep = 0.7/len(self.__reslist)
        for i in range(len(self.__reslist)-1):
            ax.plot(self.__t, self.__reslist[i][:, 0],
                    color=str(0.1+(0.7-i*colstep)))

        ax.plot(self.__t, self.__target[:, 0], color='r', lw=2, ls='--')  # Daten
        ax.plot(self.__t, self.__reslist[-1][:, 0], color='b', lw=1.5)  # Optimiertes Modell
        ax.grid(ls='--')
        ax.set_xlabel('Zeit')
        ax.set_ylabel('Weg')
        return fig

    # String-Ausgabe des Lösers
    def __str__(self):  # Magic Method
        return 'OptiSolve: %.1fkg - %.1fm' % (self.M2, self.Length)


if __name__ is '__main__':
    # Initialisiere Solver
    sol = Solver('messdaten.npy')
    # Optimierung
    sol.optimize_model((0.5, 0.7))
    # Ausgabe der Optimierung
    print('Ergebnis:', sol.M2, sol.Length)
    sol.fSize = (8, 4)
    # Plotten der optimierung
    f = sol.plotoptisteps()
    # abspeichern des bildes
    f.savefig('opti.pdf')

    # drucken der finalen Lösung des Solvers
    print(sol)  # --> print(str(sol))
