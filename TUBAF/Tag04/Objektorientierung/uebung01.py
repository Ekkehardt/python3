# -*- coding: utf-8 -*-

# Basisklasse
class Auto:
    pass  # definition einer leeren Klasse
    
class auto:
    def __init__(self, masse=5, vel=0):
        self._m = masse  # protected
        # self.__m = masse  # private
        # self.m = masse  # public
        self._v = vel
        
    def beschl(self, voffset):
        if voffset <= 100 and self._v+voffset <= 200:
            self._v += voffset
        else:
            print('Das geht nicht!')
    
    def Masse(self):
        return self._m
    
    @property  # decorator
    def MasseE(self):
        return self._m
    
    def _get_v(self):
        return self._v
    
    def _set_v(self, v):
        if v >= 0 and v <= 200:
            self._v = v
    
    Geschwindigkeit = property(_get_v, beschl)

    Geschwindigkeit1 = property(_get_v, _set_v)
    
    # MasseE = property(Masse)
    
class kombi(auto):
    def __init__(self, masse=5, vel=10, form='Kombi'):
        auto.__init__(self, masse, vel)  # aufruf basisklassenkonstruktor zwingend notwendig!
        self._heckform = form

a1 = Auto()
a2 = auto(2.5, 0.5)
a2.beschl(50.0)
a2.beschl(90.0)
a2.beschl(90.0)

a2.Geschwindigkeit = 20  # a2.beschl(20)

a2.Geschwindigkeit1 = 0

print('Masse von a2:', a2.Masse())
print('Masse von a2:', a2.MasseE)
print('Geschw. von a2:', a2.Geschwindigkeit)

k1 = kombi(3, 0)
k2 = kombi(2.8, 10, 'SUV')

print('Masse von k1:', k1.MasseE)
print('Masse von k2:', k2.MasseE)