#!/usr/bin/env python3
﻿# -*- coding: utf-8 -*-

# als erstes definieren wir uns zwei Zahlen
z1,z2 = 100,32

# jetzt noch ein wenig Spaß mit den Zahlen
z1-=32
z2+=54

# man beachte die Multiplikation / Division mit einer Gleitkommazahl damit das Ergebnis auch eine Gleitkommazahl ist
z1/=3.0
z2*=0.3

# welche Zahl ist nun größer?
if (z1>z2):
    print(z1, ' > ', z2)
else:
    print(z1, ' <= ', z2)
    
# was passiert nun hier?
if (not (z1>z2)):
    pass # hier sinnvoll ergänzen zu dem oberen Beispiel


# etwas komplizierter wird es wenn wir die Bool'sche Algebra nutzen
# siehe hierzu auch: http://de.wikipedia.org/wiki/Boolesche_Algebra
x = True
y = True
z = True

if x and y or ((y and z) and not x):
    print('Das war ein komplizierter Ausdruck!')
