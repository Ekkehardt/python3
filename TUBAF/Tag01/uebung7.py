#!/usr/bin/env python3
﻿# -*- coding: utf-8 -*-

# und nun kann man die verschiedenen Funktionen auch in ein Modul auslagern
# das Modul heißt newton

import newton as nw

x1 = nw.calc_newton(3)
x2 = nw.calc_newton_err(3)

print(x1,x2)
