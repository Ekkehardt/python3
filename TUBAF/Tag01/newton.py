#!/usr/bin/env python3
﻿# -*- coding: utf-8 -*-

# wir haben die Funktion mit Standardargumenten versehen
def calc_newton(a, fehler = 1e-6):
    x1 = (1+a)/2.0
    x2 = x1-(1-a/x1**2)/(2*a/x1**3)

    while (abs(x2-x1)>=fehler):
        x1=x2
        x2 = x1-(1-a/x1**2)/(2*a/x1**3)
    
    return x2

# diese Funktion gibt 0 zurück, wenn a nicht in dem definierten Wertebereich liegt
# andernfalls wird die berechnete Nullstelle zurück geliefert
# zudem besitzt diese Funktion einen "Dummywert" für die zu erreichende Genauigkeit.
# dieser Wert wird genutzt falls bei dem Aufruf der Funktion nur eine Variable
# übergeben wird

def calc_newton_err(a, fehler=1e-6):
    if (a<=0):
        return 0.0
    x1 = (1+a)/2.0
    x2 = x1-(1-a/x1**2)/(2*a/x1**3)
    print fehler

    while (abs(x2-x1)>=fehler):
        x1 = x2
        x2 = x1-(1-a/x1**2)/(2*a/x1**3)
    
    return x2
    
if __name__ == '__main__':
	pass
