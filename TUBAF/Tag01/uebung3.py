#!/usr/bin/env python3
﻿# -*- coding: utf-8 -*-

# als erstes definieren wir uns eine Liste mit Elementen
liste1 = [1,2,3,4,5,6,7,8,9,10]

# nun wollen wir uns alle Einträge ausgeben lassen - mittels for-Schleife
for elem in liste1:
    print(elem)

# das ganze wäre natürlich auch so gagangen:
# print(liste1)

# nun lassen sich Listenelemente wie in MATLAB auch "slicen"
# list[index_anfang:index_ende+1]
print(liste1[0:3]) # gibt die ersten drei Zahlen aus --> an Index 0,1,2
print(liste1[::-1]) # nun die ganze Liste rückwärts durchlaiufen, denn:
# list[index_anfang:index_ende+1:step]

# und die letzen beiden Elemente in der Liste?
print(liste1[:-3:-1])

# aber das Gibt es natürlich nicht nur für Zahlen, sondern für beliebige Listen und gemixten Wertetypen
liste2 = ['Hallo','Welt!']
print(liste2)

# man kann mit Listen noch viel mehr machen ... das soll es damit aber vorab gewesen sein
# wie stellt man nun eine Zählschleife dar?
# DIE range()-Funktion
for i in range(0,101,2): # Eine Zählschleife in Zweier-Schritten von 0 bis 100
    print('%d^2 = %d' % (i,i**2)) # Ausgabe der quadrierten Zahlen
