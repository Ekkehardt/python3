#!/usr/bin/env python3
﻿# -*- coding: utf-8 -*-

# viel wichtiger sind while-Schleifen als Benutzermenü
# oder allgemeiner, wenn vorher noch nicht bekannt ist, wie viele Programmläufe benötigt werden
auswahl = 10

while (auswahl != 0):
	print('Benutzermenue')
	print('1 - Eingabe')
	print('0 - Beenden')
    
	auswahl = input('Ihre Wahl: ') # vom Benutzer die passende Zahl abfangen
	if (auswahl == 1):
		print('Daten eingeben - DUMMY')
	elif (auswahl == 0):
		print('Programm wird beendet!')
		# break --> nicht nötig
	else:
		pass
    
# ein hilfreiches Beispiel fürt die while-Schleife ist die Berechnung der Nullstellen einer Funktion nach dem Newton-Verfahren
# siehe auch http://de.wikipedia.org/wiki/Newton-Verfahren

# die Funktion lautet f(x) = 1-a/x^2
# die Ableitung davon ist f'(x) = 2a/x^3
# Nullstellen sind +- sqrt(a) --> http://www.wolframalpha.com/input/?i=1-a%2Fx%5E2
# für ein a > 0 ist das Newton-Verfahrn zur Suche der Nullstelle definiert nach:
# x_{n+1} = x_n - (1-a/x_n^2)/(2a/x_n^3)
# wir iterieren solange nach einer neuen Nullstelle solange der Fehler noch größer als 1e-4 ist
# Startwert für das Newton-Verfahren lautet x_0 = (1+a)/2
a = 3.0
fehler = 1e-6
x1 = (1+a)/2.0
x2 = x1-(1-a/x1**2)/(2*a/x1**3)
it = 1
print('Iterationsschritt: %d - %1.4f' % (it,x2))

while (abs(x2-x1)>=fehler):
    x1=x2
    x2 = x1-(1-a/x1**2)/(2*a/x1**3)
    it+=1
    print('Iterationsschritt: %d - %e (Fehler: %e)' % (it,x2, abs(x2-x1)))

