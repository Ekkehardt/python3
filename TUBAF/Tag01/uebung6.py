#!/usr/bin/env python3
﻿# -*- coding: utf-8 -*-

# nun nehmen wir das Newton-Verfahren und bauen das in eine Funktion ein
# Funktionen müssen immer VOR der Verwendung deklariert werden!
# eine Funktion zur Berechnung der Nullstelle nach dem Newton Verfahren mit 
# Parameter a. Die verwendete Funktion ist y=1-(a/x²)
# Einen Überblick mit Zitationen zum Newton Verfahren bietet die Wikipediaseite:
# http://de.wikipedia.org/wiki/Newton-Verfahren

def newton1(a):
    fehler = 1e-4
    x1 = (1+a)/2.0
    x2 = x1-(1-a/x1**2)/(2*a/x1**3)
    it = 1
    print('Iterationsschritt: %d - %1.5f' % (it,x2))

    while (abs(x2-x1)>=fehler):
        x1=x2
        x2 = x1-(1-a/x1**2)/(2*a/x1**3)
        it+=1
        print('Iterationsschritt: %d - %1.5f' % (it,x2))

# eine Funktion mit zwei Parametern        
def newton2(a,fehler):
    x1 = (1+a)/2.0
    x2 = x1-(1-a/x1**2)/(2*a/x1**3)
    it = 1
    print('Iterationsschritt: %d - %1.5f' % (it,x2))

    while (abs(x2-x1)>=fehler):
        x1=x2
        x2 = x1-(1-a/x1**2)/(2*a/x1**3)
        it+=1
        print('Iterationsschritt: %d - %1.10f' % (it,x2))

# eine funktion mit zwei Parametern und der Rückgabe eines Wertes        
def newton3(a, fehler):
    x1 = (1+a)/2.0
    x2 = x1-(1-a/x1**2)/(2*a/x1**3)
    it = 1
    while (abs(x2-x1)>=fehler):
        x1=x2
        x2 = x1-(1-a/x1**2)/(2*a/x1**3)
        it += 1
    return x2
	
def newton4(a, fehler):
    x1 = (1+a)/2.0
    x2 = x1-(1-a/x1**2)/(2*a/x1**3)
    it = 1
    while (abs(x2-x1)>=fehler):
        x1=x2
        x2 = x1-(1-a/x1**2)/(2*a/x1**3)
        it += 1
    # alternativ auch als Liste:
    return [it, x2]

if __name__ is '__main__':
	# nun können die Funktionen einfach aufgerufen werden!
	newton1(3)

	newton2(3,1e-10)

	xn = newton3(3,1e-10)
	print('x_n = %1.10f' % (xn))

	it, xn = newton4(3, 1e-6)
	print('x_n = %1.10f bei %d Iterationen' % (xn, it))

	_, xn = newton4(3, 1e-6)
	print('x_n = %1.10f' % (xn))
