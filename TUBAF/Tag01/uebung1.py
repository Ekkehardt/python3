#!/usr/bin/env python3
﻿# -*- coding: utf-8 -*-

# einfache Ausgabe mit dem print - Befehl
print('Das ist ein einfaches Skript!\nEs wird vom Compiler uebersetzt und vom Interpreter ausgefuehrt!')

# Variablen lassen sich ohne einen Typ anzugeben anlegen
zahl1 = 10

# Welchen Typ hat die Variable 'zahl' jetzt bekommen
print('Typ der Variablen zahl1: ', type(zahl1))

# nehmen wir noch eine weitere Zahl hinzu
zahl2 = 3

# eine kürzere Schreibweise wäre
zahl1, zahl2 = 10, 3

# diese ist vom Typ
print('Typ der Variablen zahl2: ', type(zahl2))

# was passiert bei zahl1/zahl2?
erg1 = zahl1/zahl2
print(zahl1, ' / ', zahl2, ' = ', erg1)
print('Typ der Variablen erg1: ', type(erg1))

erg2 = zahl1/(float(zahl2))
print(zahl1, ' / ', zahl2, ' = ', erg2)
print('Typ der Variablen erg2: ', type(erg2))

# eine andere Schreibweise für die Ausgabe wäre noch:
print('%1.2f / %1.2f = %1.2f' % (zahl1,zahl2,erg2))
