#!/usr/bin/env python3
﻿# -*- coding: utf-8 -*-

# nehmen wir als Beispiel diese for-Schleife
print('for-Schleife')
for i in range(0,51,2):
    print(i)
    
# nun kann man diese auch in eine while-Schleife umschreiben
print('while-Schleife')
val = 0 # Startwert der Schleife, da man keinen in der while-Schliefe eingebaute Zählvariable hat

while (val<=50):
    print(val)
    val+=2 # hier Einrückung nicht vergessen, sonst erhält man eine Dauerschleife!!!

# ERGO: Man kann auch jede for-Schleife als while-Schleife umschreiben!
