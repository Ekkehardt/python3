# -*- coding: utf-8 -*-

import cython
import math
from libc.math cimport (atan, M_PI, sqrt)

def slope_cython1(indata, outdata):
    I = outdata.shape[0]
    J = outdata.shape[1]
    
    for i in range(I):
        for j in range(J):
            ddx = (indata[i+1, j] - indata[i+1, j+2])/2
            ddy = (indata[i, j+1] - indata[i+2, j+1])/2
            slp = math.sqrt(ddx**2 + ddy**2)
            outdata[i, j] = math.atan(slp)*180/math.pi

def slope_cython2(double [:, :] indata, double [:, :] outdata):
    cdef int I, J
    cdef int i, j
    cdef double slp, ddx, ddy

    I = outdata.shape[0]
    J = outdata.shape[1]
    
    for i in range(I):
        for j in range(J):
            ddx = (indata[i+1, j] - indata[i+1, j+2])/2
            ddy = (indata[i, j+1] - indata[i+2, j+1])/2
            slp = sqrt(ddx*ddx + ddy*ddy)
            outdata[i, j] = atan(slp)*180/M_PI

from cython.parallel import prange, parallel

@cython.boundscheck(False)
def slope_cython3(double [:, :] indata, double [:, :] outdata):
    cdef int I, J
    cdef int i, j
    cdef double slp, ddx, ddy

    I = outdata.shape[0]
    J = outdata.shape[1]
    
    with nogil, parallel(num_threads=2):
        for i in prange(I, schedule='dynamic'):
            for j in range(J):
                ddx = (indata[i+1, j] - indata[i+1, j+2])/2
                ddy = (indata[i, j+1] - indata[i+2, j+1])/2
                slp = sqrt(ddx*ddx + ddy*ddy)
                outdata[i, j] = atan(slp)*180/M_PI




            