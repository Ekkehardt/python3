#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author: Christoph Statz and Gabriel Arnold
        base on code by Prabhu Ramachandran <prabhu_r at users dot sf dot net>

License: BSD
"""

import numpy
from scipy import weave
from scipy.weave import converters
import sys, time

import pylab

import pyximport
pyximport.install(setup_args={'include_dirs':[numpy.get_include()]})
from cython_update import update


class Grid:
    
    def __init__(self, nx=10, ny=10, xmin=0.0, xmax=1.0,
                 ymin=0.0, ymax=1.0):
        self.xmin, self.xmax, self.ymin, self.ymax = xmin, xmax, ymin, ymax
        self.dx = float(xmax-xmin)/(nx-1)
        self.dy = float(ymax-ymin)/(ny-1)
        self.u = numpy.zeros((nx, ny), 'd')
        self.old_u = self.u.copy()        

    def set_bc(self, l, r, b, t):        
        self.u[0, :] = l
        self.u[-1, :] = r
        self.u[:, 0] = b
        self.u[:,-1] = t
        self.old_u = self.u.copy()

    def set_bc_function(self, func):
        xmin, ymin = self.xmin, self.ymin
        xmax, ymax = self.xmax, self.ymax
        x = numpy.arange(xmin, xmax + self.dx*0.5, self.dx)
        y = numpy.arange(ymin, ymax + self.dy*0.5, self.dy)
        self.u[0 ,:] = func(xmin,y)
        self.u[-1,:] = func(xmax,y)
        self.u[:, 0] = func(x,ymin)
        self.u[:,-1] = func(x,ymax)

    def compute_error(self):        
        v = (self.u - self.old_u).flat
        return numpy.sqrt(numpy.dot(v,v))
    

class LaplaceSolver(object):
    
    def __init__(self, grid):
        self.grid = grid

    def step(self, dt=0.0):

        g = self.grid
        nx, ny = g.u.shape        
        dx2, dy2 = g.dx**2, g.dy**2
        dnr_inv = 0.5/(dx2 + dy2)
        u = g.u

        err = 0.0

        for i in range(1, nx-1):
            for j in range(1, ny-1):
                tmp = u[i,j]

                u[i,j] = ((u[i-1, j] + u[i+1, j])*dy2 +
                          (u[i, j-1] + u[i, j+1])*dx2)*dnr_inv

                diff = u[i,j] - tmp

                err += diff*diff

        return numpy.sqrt(err)

    def solve(self, n_iter=0, eps=1.0e-16):        

        err = self.step()
        count = 1

        while err > eps:
            if n_iter and count >= n_iter:
                return err
            err = self.step()
            count = count + 1

        return count

class NumpyLaplaceSolver(LaplaceSolver):

    def __init__(self, grid):
        LaplaceSolver.__init__(self, grid)
        
    def step(self, dt=0.0):
        g = self.grid
        dx2, dy2 = g.dx**2, g.dy**2
        dnr_inv = 0.5/(dx2 + dy2)
        u = g.u
        g.old_u = u.copy()

        u[1:-1, 1:-1] = ((u[0:-2, 1:-1] + u[2:, 1:-1])*dy2 + 
                         (u[1:-1,0:-2] + u[1:-1, 2:])*dx2)*dnr_inv
        
        return g.compute_error()

class BlitzLaplaceSolver(LaplaceSolver):

    def __init__(self, grid):
        LaplaceSolver.__init__(self, grid)

    def step(self, dt=0.0):        

        g = self.grid
        dx2, dy2 = g.dx**2, g.dy**2
        dnr_inv = 0.5/(dx2 + dy2)
        u = g.u
        g.old_u = u.copy()

        expr = "u[1:-1, 1:-1] = ((u[0:-2, 1:-1] + u[2:, 1:-1])*dy2 + "\
               "(u[1:-1,0:-2] + u[1:-1, 2:])*dx2)*dnr_inv"
        weave.blitz(expr, check_size=0)

        return g.compute_error()


class CythonLaplaceSolver(LaplaceSolver):

    def __init__(self, grid):
        LaplaceSolver.__init__(self, grid)

        from cython_update import update
        self.update = update 

    def step(self, dt=0.0):        

        g = self.grid
        dx2, dy2 = g.dx**2, g.dy**2
        dnr_inv = 0.5/(dx2 + dy2)
        u = g.u
        g.old_u = u.copy()

        self.update(u, dx2, dy2) 

        return g.compute_error()


def bc(x, y):    
    return (x**2 - y**2)


def times(nmin=10, nmax=100, dn=5, eps=1.0e-16, n_iter=10):

    iters = []
    n_grd = numpy.arange(nmin, nmax, dn)

    times = []

    for i in n_grd:

        grid = Grid(nx=i, ny=i)
        grid.set_bc_function(bc)
        solver = NumpyLaplaceSolver(grid)
        t_start = time.clock()
        iters.append(solver.solve(n_iter=n_iter, eps=eps))
        dt = time.clock() - t_start
        times.append(dt)

        print "Solution for nx = ny = %d, took %f seconds"%(i, dt)

    return (n_grd**2, iters, times)


def run_time(Solver, nx=100, ny=100, eps=1.0e-16, n_iter=10):

    grid = Grid(nx, ny)
    grid.set_bc_function(bc)
    solver = Solver(grid)

    t = time.clock()
    solver.solve(n_iter=n_iter, eps=eps)
    return time.clock() - t

def run_visualization(Solver, nx=100, ny=100, n_iter=1000):

    pylab.ion()
    pylab.figure(1)
    pylab.subplot(111)
    pylab.grid(True)
    pylab.xlabel('$Cells$')
    pylab.ylabel('$T$')
    line1, = pylab.plot([0,nx],[0,1],'k-')

    grid = Grid(nx, ny)
    grid.set_bc_function(bc)
    solver = Solver(grid)

    k=numpy.arange(0,nx)

    pylab.show()

    for n in range(0,n_iter):
        solver.step()
        line1.set_data(k,1.0*solver.grid.u[:,50])
        pylab.draw()

    raw_input('Done?')
      
def main():


    times()
    run_visualization(CythonLaplaceSolver)
    print(run_time(NumpyLaplaceSolver))
    print(run_time(CythonLaplaceSolver))


if __name__ == "__main__":
    main()
