# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from numpy.lib import pad
from timeit import Timer

import pyximport
pyximport.install(setup_args={"script_args": ["--compiler=mingw32"],
                              "include_dirs": np.get_include()},
                  reload_support=True,
                  build_in_temp=False)
from slope_cython import slope_cython1, slope_cython2, slope_cython3

imfile = 'TDX_Vulkane_xl.jpg'
indata = plt.imread(imfile)

def rgb2gray(rgb):
    return np.abs(np.dot(rgb[..., :3],
                         [0.299, 0.587, 0.114]) - 512)

grayscale = rgb2gray(indata)

outdata = np.zeros_like(grayscale)
grayscale = pad(grayscale, (1, 1),
                'reflect', reflect_type='odd')

plt.close('all')
figsize = (9, 7)
plt.figure()
cs = plt.imshow(grayscale, cmap=plt.cm.gray)
cb = plt.colorbar(cs)
cb.set_label('Elevation')
plt.title('DEM Model')
plt.xlabel('x')
plt.ylabel('y')
plt.axis('image')
plt.gcf().set_size_inches(figsize)
plt.gca().tick_params(direction='out', which='both')
plt.tight_layout()
plt.savefig('DEM.pdf')

ti1 = Timer(lambda: slope_cython1(grayscale, outdata))
ti2 = Timer(lambda: slope_cython2(grayscale, outdata))
ti3 = Timer(lambda: slope_cython3(grayscale, outdata))
print('cython1:', ti1.timeit(number=10)/10)
print('cython2:', ti2.timeit(number=10)/10)
print('cython3:', ti3.timeit(number=10)/10)


