#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import scipy as sc
from scipy.interpolate import griddata, interp2d
import sys
import pylab as pl # das wichtigste von Matplotlib
pl.close('all')

######## 1.

daten = np.loadtxt('messdaten.dat')
tt, x1, u, I = daten.T  # 2d-Array entpacken
# tt, x1, u, I = np.loadtxt('messdaten.dat', unpack=True)
# Daten anschauen
if True:  # ( 0 -> nicht ausführen)
    pl.figure()
    pl.subplot(311)
    pl.plot(tt, x1)
    pl.xlabel('Zeit')
    pl.ylabel('Weg')
    pl.subplot(312)
    pl.plot(tt, u)
    pl.xlabel('Zeit')
    pl.ylabel('Spannung')
    pl.subplot(313)
    pl.plot(tt, I)
    pl.xlabel('Zeit')
    pl.ylabel('Strom')

    pl.show()
    # sys.exit()

# 2.

# Vorzeichen vom Strom anpassen:
I = I*np.sign(u)

# Hinweis: Wenn Spannung 0 -> VZ = 0 -> Strom = 0

if True:
    pl.figure()
    pl.plot(tt, I)
    pl.xlabel('Zeit')
    pl.ylabel('Strom')

    pl.show()
    # sys.exit()

######## 3.

# Indizes der Spannungsimpulse herausfinden

udiff = np.diff(u)
change_indices = np.arange(len(u)-1)[udiff!=0]
# Indizes der Werte True, wo es eine Änderung in U gibt
# (BOOL-Indizierung wurde benutzt)

# Verhindern, dass ein evtl "halber Impuls am Ende mit erkannt wird"

if len(change_indices) % 2 == 1:
    change_indices = change_indices[:-1]


# Bis jetzt sind die Indizes hintereinander
# Wir wollen immer zwei in einer Zeile
#1: Impuls-Start-Index, 2: Impuls-End-Index)

change_indices = change_indices.reshape(-1, 2) # -1  sagt: "so, dass es passt"

# erste Spalte noch um eins erhöhen, weil sich der Index auf den letzten
# Wert vor dem Sprung bezieht.

change_indices[:,0] += 1

print(change_indices)

######### 4.

# Histogramm vom dritten "Strom-Block" anlegen:
if True:
    i1, i2 = change_indices[2, :]

    pl.figure()
    pl.hist(I[i1:i2], bins=10)
    pl.show()
    # sys.exit()

######### 5.

# Strom mitteln:

I_mean = 0*I # neues ('leeres') array anlegen
# I_mean = np.zeros_like(I)

# Zeilenweise über change_indices iterieren
for i1, i2 in change_indices:
    I_mean[i1:i2] = np.mean(I[i1:i2]) # mittelwert bilden und speichern

if True:

    pl.figure()
    pl.plot(tt, I)
    pl.plot(tt, I_mean)

    pl.show()
    # sys.exit()

####### 6.

if True:
    pl.figure()

    start_idcs = change_indices[:,0] # erste Spalte: Indizes, wo es los geht

    # Für jeden Strom-Block ein Spannungs-Strom-Werte-Paar bestimmen:
    ii = I_mean[start_idcs]
    uu = u[start_idcs]

    pl.plot(uu, ii, 'bx', ms = 7) # große blaue Kreuze (x)

    a1, a0 =  sc.polyfit(uu, ii, 1) # lineare regression

    pl.plot(uu, a1*uu+a0, 'g-') # Polynom (Geradengleichung) auswerten und plotten
    # alternativ: sc.polyval [a1, a0]

    print("Leitwert:", a1)
    print("Strom-Offset", a0)

    pl.show()
    # sys.exit()

######### 7.

dt = np.mean(np.diff(tt))
xd = np.diff(x1)/dt
xdd = np.diff(xd)/dt
# xdd = np.diff(x1, 2)/dt**2

######### 8.
if True:
    pl.figure()
    for i1, i2 in change_indices:

        # wir wollen jetzt nur positive Spannungsimpulse
        # wenn u< 0 ist: mit nächstem Schleifendurchlauf weitermachen
        if u[i1] < 0: continue

        pl.plot(xd[i1:i2], xdd[i1:i2])

    pl.show()
    # sys.exit()

######### 9.

# siehe
# http://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.griddata.html

# Wir brauchen die Eingangsdaten in folgendem Format:

# points (Shape = (N, 2)) Jede Zeile ist ein Punkt im Geschw.-Beschl.-Diagramm
# voltage (len = N), der zum jeweiligen Punkt gehörende Spannungswert

# wir arbeiten erstmal mit Listen und wandeln am Ende in arrays um

points_vel = []
points_acc = []
voltage = []

for i1, i2 in change_indices:

    # negative Werte ignorieren
    if u[i1] < 0: continue

    points_vel += list(xd[i1:i2-1])
    points_acc += list(xdd[i1:i2-1])

    # Liste der entsprechenden Länge, in der alle Elemente den gleichen Wert haben
    # nämlich den passenden Spannungswert
    voltage += [u[i1]]*(i2-1-i1)


# pseudo-Werte am Rand hinzufügen um nan zu vermeiden
# Annahme: bei 3V bewegt sich noch (fast) nichts
points_vel=[0, 0,   0, 7, 7] + points_vel
points_acc=[0, 3, 14, 0, 14] + points_acc
voltage = [3, 3, 12, 12, 12] + voltage

# Listen als Arrays zusammenpacken:
points = np.array([points_vel, points_acc]).T
voltage = np.array(voltage)


xd_max = max(points[:,0])
xdd_max = max(points[:,1])
N_grid = 100

# reguläres Gitter erzeugen
grid_v_arr1d = np.linspace(0, xd_max, N_grid)
grid_a_arr1d = np.linspace(0, xdd_max, N_grid)
vv, aa = np.meshgrid(grid_v_arr1d, grid_a_arr1d) # 2d arrays


# Interpolation durchfürhen
interp_voltage = griddata(points, voltage, (vv, aa))

# NAN am Rand eleminieren
interp_voltage[:, 0] = interp_voltage[:, 1] # erste Spalte := zweite Spalte
interp_voltage[0, :] = interp_voltage[1, :] # erste Zeile:= zweite Zeile

if True:
    # Datenfeld grafisch darstellen
    pl.figure()
    pl.imshow(interp_voltage, extent=(0,xd_max,0,xdd_max), origin='lower', interpolation='nearest')
    cb = pl.colorbar()
    cb.set_label('Spannung')


    # nochmal die Linien zeichnen
    for i1, i2 in change_indices:

        if u[i1] < 0: continue

        pl.plot(xd[i1:i2-1], xdd[i1:i2-1], 'k-')


    pl.show()
    # sys.exit()
