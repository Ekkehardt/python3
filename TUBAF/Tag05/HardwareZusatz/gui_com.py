#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import PyQt4.QtCore as QtCore
import PyQt4.QtGui as QtGui

import sys
import comschnittstelle as com

# QApplication Instanz wird immer benoetigt (sys.argv erst mal hinnehmen)
app = QtGui.QApplication(sys.argv)
app.setFont(QtGui.QFont('Calibri',12))

#-------------------------------------------------------------------------------
# Dialog erzeugen
dialog = QtGui.QDialog()
dialog.setWindowTitle('COM-Schnittstelle')
# Variable für COM-Port
COM = com.COMProtocol('COM6')
# Label und Eingabefelder
mass1Label = QtGui.QLabel('Probennummer', dialog)
mass1Edit = QtGui.QLineEdit('1', dialog)

mass2Label = QtGui.QLabel('Eintauchschritte', dialog)
mass2Edit = QtGui.QLineEdit('100', dialog)


# Buttons
simButton = QtGui.QPushButton('Initialisieren', dialog)
openButton = QtGui.QPushButton('Gehe zu Probe', dialog)
saveButton = QtGui.QPushButton('Tauche ein in Probe', dialog)
mischButton = QtGui.QPushButton('Gehe zu Mischstation', dialog)
exitButton = QtGui.QPushButton('Exit', dialog)

# Ausrichtung anpassen
mass1Edit.setAlignment(QtCore.Qt.AlignRight)
mass2Edit.setAlignment(QtCore.Qt.AlignRight)

# zulaessige Zeichen beschraenken - hier nur Zahlen eingeben
mass1Edit.setValidator(QtGui.QIntValidator(mass1Edit))
mass2Edit.setValidator(QtGui.QIntValidator(mass2Edit))

# Layout
layout = QtGui.QGridLayout()
layout.addWidget(mass1Label, 0,0)
layout.addWidget(mass1Edit, 0,1)
layout.addWidget(mass2Label, 1,0)
layout.addWidget(mass2Edit, 1,1)

layout.addWidget(simButton, 3,1, 1,1, QtCore.Qt.AlignRight)
layout.addWidget(openButton, 4,1, 1,1, QtCore.Qt.AlignRight)
layout.addWidget(saveButton, 5,1, 1,1, QtCore.Qt.AlignRight)
layout.addWidget(mischButton, 6,1, 1,1, QtCore.Qt.AlignRight)
layout.addWidget(exitButton, 7,1, 1,1, QtCore.Qt.AlignRight)

dialog.setLayout(layout)

# Focus auf Exit
exitButton.setFocus()

# Funktionen definieren
# Geräte initialisieren
def initialisiere():
    # das Gerät hängt am COM5-Port --> siehe Gerätemanager unter Windows
    # für die Kommunikation sind folgende Einstellungen zu Verwenden
    # 9600 Baud
    # 7 Datenbits
    # Parität ungerade
    # 1 Stoppbit

    COM.getStatus()
    COM.init()

def geheProbe():
    pr = int(str(mass1Edit.text()))
    # tiefe = int(str(mass2Edit.text()))
    COM.probe(pr)
        
def taucheProbe():
    pr = int(str(mass1Edit.text()))
    t = int(str(mass2Edit.text()))
    COM.gehe_tauche(pr,t)
    
def mischstation():
    COM.mische()
    COM.tauche(500)
# Buttons verknuepfen
simButton.clicked.connect(initialisiere)
openButton.clicked.connect(geheProbe)
saveButton.clicked.connect(taucheProbe)
mischButton.clicked.connect(mischstation)
exitButton.clicked.connect(dialog.close)

#-------------------------------------------------------------------------------

# Dialog anzeigen
dialog.exec_()
