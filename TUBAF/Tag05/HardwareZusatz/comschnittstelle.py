#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# COM - Schnittstelle Kommunikation
# Kommunikation mit Probenspeicher

import serial # Paket für die Kommunikation über COM einbinden

class COMProtocol:
    def __init__(self,comport='COM1',byt=serial.SEVENBITS,par=serial.PARITY_ODD):
        self.s = serial.Serial(comport, bytesize=byt, parity=par)
        print 'Geraet arbeitet am Port ' + self.s.name
        # self.init()
        
    def getStatus(self):
        statuscmd = 's\r'
        self.s.write(statuscmd)
        buf = self.s.read()
        res = buf
        while buf != '\r':
            buf = self.s.read()
            res+=buf
        print 'Status: ' + str(res)

    def init(self):
        self.s.write('I\r')
        buf = self.s.read()
        res = buf
        while buf != '\r':
            buf = self.s.read()
            res+=buf
        print 'Initialisierungsphase: ' + str(res)
    
    def probe(self,n):
        self.s.write('G'+str(n)+'\r')
        buf = self.s.read()
        res = buf
        while buf != '\r':
            buf = self.s.read()
            res+=buf
        print 'Gehe zu Probe ' + str(n) + ': ' + str(res)
    
    def tauche(self,t):
        self.s.write('Ta'+str(t)+'\r')
        buf = self.s.read()
        res = buf
        while buf != '\r':
            buf = self.s.read()
            res+=buf
        print 'Tauche in Probe um ' + str(t*0.125) + 'mm ein: ' + str(res)
    
    def mische(self):
        self.s.write('GSp\r')
        buf = self.s.read()
        res = buf
        while buf != '\r':
            buf = self.s.read()
            res+=buf
        print 'Gehe zu externer Mischposition: ' + str(res)

    def gehe_tauche(self,n,t):
        self.probe(n)
        self.tauche(t)
        
    def __del__(self):
        if self.s.isOpen():
            self.s.close()
