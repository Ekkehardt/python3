#define DllExport __declspec(dllexport)

#include <stdio.h>
#include <stdlib.h>

DllExport void bubblesort(int *array, int elemente) {
   int i,temp;

   while(elemente--)
      for(i = 1; i <= elemente; i++)
         if(array[i-1] > array[i]) {
            temp=array[i];
            array[i]=array[i-1];
            array[i-1]=temp;
         }
}