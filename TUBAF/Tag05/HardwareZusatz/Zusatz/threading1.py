#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import _thread

def naehere_pi_an(n):
    '''
    Näherungsweise Berehcnung der Kreiszahl pi in n Schritten
    '''
    pi_halbe = 1 
    zaehler, nenner = 2.0, 1.0 
 
    for i in range(n): 
        pi_halbe *= zaehler / nenner 
        if i % 2: 
            zaehler += 2 
        else: 
            nenner += 2 
 
    print("Annaeherung mit %d Faktoren: %.16f" % (n, 2*pi_halbe))
    
if __name__ == '__main__':
    # Annäherung mit 1000 Schritten
    naehere_pi_an(1000)
    
    # Nun gleichgzeitig mehrere Funktionen laufen lassen
    _thread.start_new_thread(naehere_pi_an, (1000000, ))
    _thread.start_new_thread(naehere_pi_an, (10000, ))
    _thread.start_new_thread(naehere_pi_an, (100000, ))

    while True: # Unschön, wel es muss per Hand abgebrochen werden
        pass
    
