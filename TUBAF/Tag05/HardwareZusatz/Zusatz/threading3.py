#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import threading 
 
class PiBerechnenThread(threading.Thread): 
    def __init__(self, n): 
        threading.Thread.__init__(self) 
        self.__n = n 
 
    def run(self): 
        pi_halbe = 1 
        zaehler, nenner = 2.0, 1.0 
 
        for i in range(self.__n): 
            pi_halbe *= zaehler / nenner 
            if i % 2: 
                zaehler += 2 
            else: 
                nenner += 2 
 
        print("Annaeherung mit %d Faktoren: %.16f" % (self.__n, 2*pi_halbe))
 
meine_threads = [] 
 
while True: 
    eingabe = int(input('Bitte Anzahl Schritte eingeben (<=0 fuer beenden): '))

    if eingabe <= 0: 
        break
    else:
        thread = PiBerechnenThread(int(eingabe)) 
        meine_threads.append(thread) 
        thread.start() 
 
for t in meine_threads: 
    t.join()
