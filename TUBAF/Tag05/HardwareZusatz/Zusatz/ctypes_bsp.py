#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from ctypes import CDLL, c_int
import random
import numpy as np

# Zahlen anlegen
zahlen = np.arange(1, 101, dtype=np.int32)
print(zahlen)

random.shuffle(zahlen)
print(zahlen)

sortdll = CDLL('sort_alg.dll')
arraytyp1 = (c_int * len(zahlen))()

for index, value in enumerate(zahlen):
    arraytyp1[index] = value

sortdll.bubblesort(arraytyp1, len(zahlen))

print([i for i in arraytyp1])
