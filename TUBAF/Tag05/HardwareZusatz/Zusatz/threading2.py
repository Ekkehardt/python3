#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import _thread

anzahl_threads = 0 
thread_gestartet = False 
 
lock = _thread.allocate_lock()

def naehere_pi_an(n):
    '''
    Näherungsweise Berehcnung der Kreiszahl pi in n Schritten
    '''
    
    # der Funktion die globalen Variablen bekannt machen
    global anzahl_threads, thread_gestartet 

    # einen Lock erbitten um globale Variablen sicher zu ändern    
    lock.acquire() 
    anzahl_threads += 1 
    thread_gestartet = True 
    lock.release() 
    # den Lock wieder entfernen
    
    pi_halbe = 1 
    zaehler, nenner = 2.0, 1.0 
 
    for i in range(n): 
        pi_halbe *= zaehler / nenner 
        if i % 2: 
            zaehler += 2 
        else: 
            nenner += 2 
 
    print("Annaeherung mit %d Faktoren: %.16f" % (n, 2*pi_halbe))
    
    # hier das gleiche nochmals da Thread beendet wird
    lock.acquire() 
    anzahl_threads -= 1 
    lock.release() 
    
if __name__ == '__main__':
    # wieder einige berechnungen starten
    _thread.start_new_thread(naehere_pi_an, (100000,)) 
    _thread.start_new_thread(naehere_pi_an, (10000,))  
    _thread.start_new_thread(naehere_pi_an, (1234569,)) 

    while not thread_gestartet: 
        pass 
 
    while anzahl_threads > 0: 
        pass
