:: The only thing you need is the Microsoft Visual C++ Build Tools --> for cl.exe
:: In the start menu please open Visual Studio Developer Command Prompt and go to the directory where
:: the file sort_alg.c resides --> now just execute this file and the dll will be created
:: for an 64bit installation of Anaconda it is necessary to compile also a 64bit dll
:: and for 32bit Anaconda you will need a 32bit dll
:: Ususally you can choose if you want to start the 64bit Developer Command Prompt or the 64bit version
:: this will also select the build architecture
@echo "Compiling dll"
cl.exe /LD sort_alg.c
::cl.exe /D_USRDLL /D_WINDLL /O2 sort_alg.c sort_alg.obj /link /DLL /OUT:sort_alg.dll