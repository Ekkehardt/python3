#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from ctypes import CDLL, c_int
import random
import numpy as np
import time


def bubbleSort(alist):
    for passnum in range(len(alist)-1, 0, -1):
        for i in range(passnum):
            if alist[i] > alist[i+1]:
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp


zahlen = np.arange(1, 1001, dtype=np.int32)
# print zahlen

random.shuffle(zahlen)
# print zahlen

sortdll = CDLL('sort_alg.dll')
arraytyp = (c_int*len(zahlen))()

# print arraytyp

for index, value in enumerate(zahlen):
    arraytyp[index] = value

t1 = time.time()
sortdll.bubblesort(arraytyp, len(zahlen))
t2 = time.time()
print('Time taken: ', t2-t1, 's')
# print [i for i in arraytyp]

t1 = time.time()
bubbleSort(zahlen)
t2 = time.time()
print('Time taken: ', t2-t1, 's')
