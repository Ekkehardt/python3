# -*- coding: utf-8 -*-

import sympy as sp
import numpy as np

a, x = sp.symbols('a, x')
# a, x = sp.symbols(['a', 'x'])

fx = 1-a/x**3+4*x**2

# 1. Ableitung von fx
fx1 = fx.diff(x)
# fx1 = sp.diff(fx, x)

print('Funktion:')
print(fx)
sp.pprint(fx)

print('Ableitung:')
print(fx1)
sp.pprint(fx1)

a_val = 3.0

print('Ersetze a mit Wert', a_val)
params_value = {a: a_val}
# zugriff auf a_val:
# params_value[a]
fx_a = fx.subs(params_value)
fx1_a = fx1.subs(params_value)

print('Funktion:')
sp.pprint(fx_a)

print('Ableitung:')
sp.pprint(fx1_a)

fxa_fnc = sp.lambdify(x, fx_a, modules='numpy')
fx1a_fnc = sp.lambdify(x, fx1_a, modules='numpy')

def newton_symbolic(fx, fx1, a, fehler=1e-6):
    x1 = a/2.0
    x2 = x1 - fx(x1)/fx1(x1)
    it = 1
    
    while abs(x2-x1)>=fehler:
        x1 = x2
        x2 = x1 - fx(x1)/fx1(x1)
        it += 1
    return (it, x2)

it, xn = newton_symbolic(fxa_fnc, fx1a_fnc, a_val)
print('Näherung für a=%.2f: %.4lf (Iterationen: %d)' % (a_val, xn, it))

from sympy.solvers import solve
print('Vergleich mit sympy solve:', solve(fx_a, x)[0]) # nur die erste (reelle Nullstelle)
erg = solve(fx_a, x) # gesamte liste mit Nullstellen