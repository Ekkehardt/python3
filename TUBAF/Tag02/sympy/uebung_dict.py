# -*- coding: utf-8 -*-

# neues leeres dictionary
di = {}

for i in range(10):
    di.update({i+1: i**2})


# zugriff auf einen Wert:
di[2] += 800

for k, v in di.items():
    print(k, v)

del di[2] # key 2 und dazugehörigen Wert löschen
