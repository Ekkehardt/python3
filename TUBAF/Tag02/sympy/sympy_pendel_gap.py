#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sympy as sp
import numpy as np
import matplotlib.pyplot as plt
sp.init_printing(use_unicode=True)
plt.close('all')

# Erhaltungssatz für math. Pendel
# E_pot + E_kin = const.
# E_pot = m*g*l*(1-cos \phi)
# E_kin = 1/2*m*(l*\phi')^2
# d/dt E = 0


def lagrange():
    m, g, le = ...
    A = sp.Symbol('A', real=True)
    t = ...
    phi = ...
    phidt = ...

    Ep = ...
    Ek = ...

    L = Ek - Ep
    Eq = ...
    print('Lagrange-Funktion:')
    sp.pprint(L)

    # Näherung für kleine Winkel phi < 10°
    rew = {sp.sin(phi(t)): phi(t)}
    Eq1 = Eq.subs(rew)
    print('Kleinwinkelnäherung:')
    sp.pprint(Eq1)

    sol = ...

    # Einführung von Abkürzungen
    om = sp.Symbol('omega', real=True)
    rew = {sp.sqrt(g/le): om}
    allg = ...
    print('Allgemeine Lösung:')
    sp.pprint(allg)

    phit = allg.rhs
    phidt = allg.rhs.diff(t)

    C1, C2 = ...
    # Einsetzen der RB
    # phi(0) = A
    # phi'(0) = 0

    # Auflösen nach C1, C2
    sc = sp.solve((phit.subs(t, 0) - A, phidt.subs(t, 0)), (C1, C2))

    sol = ...
    final = sp.Eq(phi(t), sol)
    print('Spezielle Lösung:')
    sp.pprint(final)

    phit_f = ...
    phidt_f = ...
    phiddt_f = ...

    return (phit_f, phidt_f, phiddt_f)


def plotsim(phitf, phidtf, phiddtf, f=1.0, A=0.5, tend=2.0):
    fig, ax1 = plt.subplots(1, 1)
    tvec = np.linspace(0, tend, 100)
    w = 2*np.pi*f

    ax1.plot(tvec, phitf(A, w, tvec), label=r'$\phi(t)$')
    ax1.plot(tvec, phidtf(A, w, tvec), label=r'$\frac{d}{dt}\phi(t)$')
    ax1.plot(tvec, phiddtf(A, w, tvec), label=r'$\frac{d^2}{dt^2}\phi(t)$')
    ax1.legend()
    ax1.set_xlabel('Time in $s$')


if __name__ is '__main__':
    phit_f, phidt_f, phiddt_f = lagrange()
    ...
