#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Übung zum Symbolischen Rechnen
# Beispiel entstammt von: http://de.wikipedia.org/wiki/Lagrange-Formalismus

from sympy import Function
import sympy as sp


# Hilsfunkltion zum Darstellen der Gleichung im Latex-Stil
def preview(expr, **kwargs):
    import matplotlib.pyplot as plt
    latex_str = "$ %s $" % sp.latex(expr, **kwargs)
    latex_str = latex_str.replace("operatorname", "mathrm")
    plt.text(0.5, 0.5, latex_str, fontsize=30, horizontalalignment='center')
    plt.axis('off')
    plt.show()


# Benötigte Symbole definieren
t = sp.Symbol('t')
params = sp.symbols('m, D')

# Symbole entpacken
m, D = params

# Funktionen definieren
xt = Function('x')(t)

# und Ableitungen bilden
xdt = xt.diff(t)
xddt = xt.diff(t, 2)

# kinetische Energie
T = 1/2*m*xdt**2

# Potentielle Energie
U = 1/2*D*xt**2

# Lagrange-Funktion
L = T - U

# Funktionen durch Symbole ersetzen
x_s, xd_s = sp.symbols('x_s, xd_s')


# Reihenfolge ist hier wichtig. Höhere Ableitungen zuerst substituieren.
subs_list1 = [(xdt, xd_s), (xt, x_s)]

# hier werden die Funktionen durch Symbole ersetzt
L = L.subs(subs_list1)
# Vereinfachen
L = L.expand()
L = sp.trigsimp(L)

# Ausgabe der Lagrange-Funktion
print('Langrange Funktion')
sp.pprint(L)
print('\n')

# --- Lagrange-Gleichungen ---
# Hilfsterme:

L_d_x = L.diff(x_s)
L_d_xd = L.diff(xd_s)

xdd_s = sp.Symbol('xdd_s')

# sub_slist ergänzen (wieder Reihenfolge beachten: höhere Ableitungen vorn hin)
subs_list1 = [(xddt, xdd_s)] + subs_list1

# Liste umkehren (subs Symbole -> Funktionen)
# über Lambda-Funktion
subs_list_rev = [(tup[1], tup[0]) for tup in subs_list1]
L_d_xd = L_d_xd.subs(subs_list_rev)

# Ableiten und gleich danach Funktionen -> Symbole ersetzen
DL_d_xd = L_d_xd.diff(t).subs(subs_list1)


# Gleichung
Eq1 = DL_d_xd - L_d_x
# lösen
sol = sp.solve(Eq1, xdd_s)

# Funktionen erzeugen zur Simulation
params_values = {m: 0.8, D: 0.3}
xdd_expr = sol[0].subs(params_values)

# Funktionen für die numerische Berechnung der Beschleunigungen
# xdd_fnc ist ausführbares bzw. aufrufbares-Objekt ("callable object")
xdd_fnc = sp.lambdify([x_s, xd_s], xdd_expr, 'numpy')

if __name__ == "__main__":
    # Dieser Block wird nur ausgeführt, wenn diese Datei das Haupt-Skript ist,
    # nicht wenn sie als Modul woanders importiert wird.
    # dient zur Anzeige der Ergebnisse

    # sp.Eq -> sympy Equation object (-> xdd = ... (sieht hübscher aus))
    Eq1a = sp.Eq(xdd_s, sol[0])

    # Lösung ausgeben
    print('Loesung der Lagrange-Gleichung')
    sp.pprint(Eq1a)

    # Symbolbenennung in LaTeX-Notation
    sn_dict = {xdd_s: r'\ddot{x}'}

    preview(Eq1a, symbol_names=sn_dict)
