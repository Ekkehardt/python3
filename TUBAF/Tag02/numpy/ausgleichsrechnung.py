#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
# from numpy import loadtxt
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# lade daten
t, y = np.loadtxt('messung.dat', delimiter=',', skiprows=1, unpack=True)

B = np.loadtxt('messung.dat', delimiter=',', skiprows=1)
t = B[:, 0]
y = B[:, 1]

# daten anzeigen
plt.figure()
plt.plot(t, y, 'r--', lw=2.5, marker='.', markersize=15)
plt.xlabel('t')
plt.ylabel('y')

# zu bestimmende parameter alpha, beta --> ergebnisvektor x
# Matrix A anlegen für Koeffizienten
# size A (Anzahl an Daten, Anzahl an Parameter)

A = np.ones((len(t), 2))

# erste spalte mit korrekten werten für t besetzen
A[:, 0] = 1/(1+t)

# aufstellen der normalengleichungen
# A^TAx = A^Ty
rhs = np.dot(A.T, y)
lhs = np.dot(A.T, A)
x = np.dot(np.linalg.inv(lhs), rhs)
alpha = x[0]
beta = x[1]

tn = np.linspace(0, 3)
fm = alpha*(1/(1+tn)) + beta
plt.figure()
plt.plot(t, y, 'r--', lw=2.5, marker='.', markersize=15)
plt.plot(tn, fm, 'b--', lw=2.5)
plt.xlabel('t')
plt.ylabel('y')


# curve fitting mit scipy.optimize
def func(t, alpha, beta):
    return alpha*(1/(1+t))+beta


popt, pcov = curve_fit(func, t, y)
alpha1 = popt[0]
beta1 = popt[1]
