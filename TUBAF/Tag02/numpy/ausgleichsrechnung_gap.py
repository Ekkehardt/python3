#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# lade daten
t, y = ...

# daten anzeigen
plt.figure()
plt.plot(t, y, 'r--', lw=2.5, marker='.', markersize=15)
plt.xlabel('t')
plt.ylabel('y')

# zu bestimmende parameter alpha, beta --> ergebnisvektor x
# Matrix A anlegen für Koeffizienten
# size A (Anzahl an Daten, Anzahl an Parameter)

A = ...

# erste spalte mit korrekten werten für t besetzen
A[:, 0] = 1/(1+t)

# aufstellen der normalengleichungen
# A^TAx = A^Ty
rhs = ...
lhs = ...
x = ...
alpha = x[0]
beta = x[1]

tn = np.linspace(0, 3)
fm = alpha*(1/(1+tn)) + beta
plt.figure()
plt.plot(t, y, 'r--', lw=2.5, marker='.', markersize=15)
plt.plot(tn, fm, 'b--', lw=2.5)
plt.xlabel('t')
plt.ylabel('y')


# curve fitting mit scipy.optimize
def func(t, alpha, beta):
    return alpha*(1/(1+t))+beta

popt, pcov = ...
alpha1 = popt[0]
beta1 = popt[1]
