#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from sympy_pendel import lagrange
from scipy.io import savemat, loadmat
from scipy.signal import filtfilt, butter
import pylab as pl


def simulate_measurement(le, A0, tend, g=9.81):
    phit_f, phidt_f, phiddt_f = lagrange()

    w = (g/le)**0.5
    f = w/(2*np.pi)
    print('Pendel-Frequenz: %.2f Hz' % f)

    t = np.linspace(0, tend, 5000)
    phi = phit_f(A0, w, t)
    phidt = phidt_f(A0, w, t)
    phiddt = phiddt_f(A0, w, t)

    # Hinzufügen von Rauschen in der Magnitude von 25% von A
    err = ...
    gest = ...

    # speichern der Daten als MATLAB Datei
    sdict = {'t': t, 'phi': phi,
             'phidt': phidt, 'phiddt': phiddt,
             'gest': gest}
    ...


def perform_filter(filename='results.mat'):
    # Daten laden
    di = ...
    t = di['t'][0]
    ori = ...
    sig = ...

    # Daten plotten
    pl.figure()
    pl.plot(t, ori)
    pl.plot(t, sig)
    pl.xlabel('Zeit in s')
    pl.ylabel('Amplitude')

    # Berechne FFT
    phif = np.fft.fft(ori)
    gesf = np.fft.fft(sig)
    L = len(phif)//2+1
    dt = t[1] - t[0]
    fs = 1/dt
    print('Sample interval: %.4f s' % dt)
    print('Sampling frequency: %.1f Hz' % fs)

    freq = np.linspace(0, fs/2, L)

    pl.figure()
    pl.semilogx(freq, 2/L*np.abs(phif[:L]))
    pl.semilogx(freq, 2/L*np.abs(gesf[:L]))

    # Konstruiere Butterworth-Lowpassfilter
    Wn = ... / (fs/2)  # max. Frequenz 1 Hz
    b, a = butter(3, Wn, btype='low')
    gef = filtfilt(b, a, sig)

    # plotte gefilteretes Signal
    pl.figure()
    pl.plot(t, ori)
    pl.plot(t, gef)

    # plotte Amplitudenspektrum von Signal
    geff = np.fft.fft(gef)
    pl.figure()
    pl.semilogx(freq, 2/L*np.abs(phif[:L]))
    pl.semilogx(freq, 2/L*np.abs(geff[:L]))


if __name__ is '__main__':
    sim = False
    if sim:
        simulate_measurement(2.0, 2.5, 10)
    perform_filter()
