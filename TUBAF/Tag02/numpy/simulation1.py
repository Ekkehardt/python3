#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from numpy import r_, pi
from scipy.integrate import odeint
import pylab as pl # für das Plotten am Ende

from lagrange_lsg import xdd_fnc, phidd_fnc

def rhs(z, t):
    x, phi, xd, phid = z # Entpacken
    F = 0

    xdd = xdd_fnc(x, phi, xd, phid, F)
    phidd = phidd_fnc(x, phi, xd, phid, F)

    return r_[xd, phid, xdd, phidd]

t = np.linspace(1, 10, 101)

x0 = r_[0,  pi*3/4, 0, 0]

res = odeint(rhs, x0, t)

# res: Spalten -> Zustände, Zeilen -> Zeitschritte

# Entpacken der einzelnen Zustände.
# Dazu Zeilen u. Spalten vertauschen (Transponieren)

x, phi, xd, phid = res.T

# Grafische Darstellung:
pl.plot(t, x)
#pl.plot(t, phi)
pl.show()

## "Pseudo-Messdaten" für Aufgabe 2 generieren
## der Block wird nicht ausgeführt, lässt sich aber schnell in "aktiven Code"
## umwandeln (0 durch 1 ersetzen)

if 0:
    # Binärformat:
    np.save('messdaten.npy', res)
    # Textfomat (Menschenlesbar, braucht aber mehr Speicherplatz):
    np.savetxt('messdaten.txt', res)

    print("Files written.")
