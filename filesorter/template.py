#!/usr/bin/env python
# -*- coding: utf8 -*-
#Dateien nach Erweiterung sortieren
#sort.py
import os
import os.path
import shutil

while True:
    source = raw_input('Quellverzeichnis\n')
    if os.path.exists(source): break
    else: print 'Das angegebene Verzeichnis existiert nicht.'
while True:
    destination = raw_input('Zielverzeichnis\n')
    if os.path.exists(destination): break
    else: print 'Das angegebene Verzeichnis existiert nicht.'

for root, dirs, files in os.walk(source, topdown=False):
    for files2 in files:
        extension = os.path.splitext(os.path.join(root,files2))[1][1:]
        if os.path.exists(os.path.join(destination,extension)):
            shutil.copy(os.path.join(root,files2), os.path.join(destination,extension))
        else:
            os.mkdir(os.path.join(destination,extension))
            shutil.copy(os.path.join(root,files2), os.path.join(destination,extension))

