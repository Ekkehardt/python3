#!/usr/bin/env python3
# -*- coding: utf8 -*-
#Fret Calculator
#guitar.py

ScaleLength = 0
CumulativeLength = 0

def CalcSpacing(Length,NTF):
    BridgeToFret = Length - NTF
    NutToFret = (BridgeToFret/17.817) + NTF
    return NutToFret

def DoWork(ScaleLength):
    CumulativeLength = 0
    for x in range(1,25):
        FretNumber = x
        if FretNumber == 1:
            CumulativeLength = CalcSpacing(ScaleLength,0)
        else:
            CumulativeLength = CalcSpacing(ScaleLength,CumulativeLength)
        print("Fret=%d, NutToFret=%.3f" % (FretNumber,CumulativeLength))

ScaleLength = input("Please enter Scale Length of guitar -> ")
DoWork(float(ScaleLength))
